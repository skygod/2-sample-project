# Exercise 2

In this exercise we will execute a task automatically. 

In this exercise we will use:
1. Gitlab Pipeline
1. Merge Requests
1. Trigger Pipelines manually
1. Schedule Pipeline executions

## 1: Inspect branch

* [ ] **Take a look around**

- `suites` folder contains a suite with a simple task checking current weather conditions in Helsinki
- `.gitlab-ci.yml` contains a disabled job *Check winter in Helsinki*

## 2: Create Merge Request on to develop
* [ ] **Create a merge request for this branch on to devleop**
* [ ] ** :warning: Do not execute the merge :warning:**
* [ ] **Find logs of the pipeline (will be used in next step)**


## 3: Activate disabled job

* [ ] **Activate job *Check winter in Helsinki* **
* [ ] **Execute pipeline on branch**
* [ ] **How did the activated job perform? How did the pipeline perform compared to the previous execution?**

## 4: Test the task

* [ ] **Find a way testing the task**
* [ ] **What do you think of the options for testing a productive task? Do you see any implications?**
* [ ] **Do you have suggestions for testing a productive robot task?**

There are several ways testing the task. Here are some options:

1. Execute task in a testing step in pipeline
1. Checkout repository and execute task locally
1. Execute task in testing step in pipeline with `--dryrun` option

## 5: Finish Merge Request
* [ ] **When you are certain that the task in itself is save, finish the merge request**

## 6: Schedule frequent execution
* [ ] **Schedule execution on master**

## 7: Create a merge request from develop on to master and close it
* [ ] **Create a merge request from develop on to master**
* [ ] **Make pipeline for this merge request go green**
* [ ] **Check, if you scheduled execution on master will execute the task correctly**